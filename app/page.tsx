import { Hero, SearchBar, CustomFilter } from "@/components";
import { fetchCars } from "@/utils";

export default async function Home() {
  const allCars = await fetchCars();
  const isDataEmpty = !allCars || !Array.isArray(allCars) || allCars.length < 1;
  return (
    <main className="overflow-hidden ">
      <Hero />

      <div className="mt-12 padding-x padding-y max-width" id="discover">
        <div className="home__text-container">
          <h1 className="text-4xl font-extrabold">Car Catalogue</h1>
          <p>Explore out cars you might like</p>
        </div>

        <div className="home__filters">
          <SearchBar />

          <div className="home__filter-container">
            {/* <CustomFilter title="fuel" options={fuels} />
            <CustomFilter title="year" options={yearsOfProduction} /> */}
          </div>
        </div>
        {isDataEmpty ? (
          <section> We have cars {allCars.length}</section>
        ) : (
          <section> No cars are found {allCars.length}</section>
        )}
      </div>
    </main>
  );
}
