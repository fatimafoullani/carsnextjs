const url = "https://cars-by-api-ninjas.p.rapidapi.com/v1/cars?model=corolla";

export async function fetchCars() {
  const headers = {
    "X-RapidAPI-Key": "c3613a9ef0msh738e36a03bc5fc4p1f4b6djsncf72d672eb52",
    "X-RapidAPI-Host": "cars-by-api-ninjas.p.rapidapi.com",
  };
  const response = await fetch(url, { headers: headers });
  const result = await response.text();
  return result;
}
