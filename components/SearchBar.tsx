"use client";
import Image from "next/image";
import SearchManufacturer from "./SearchManufacturer";
import React, { useState } from "react";
function SearchBar() {
  const [searchModel, setSearchModel] = useState("");
  const [searchManufacturer, setSearchManufacturer] = useState("");

  const handleSearch = () => {};
  return (
    <form className="searchbar" onSubmit={handleSearch}>
      <div className="searchbar__item">
        <SearchManufacturer
          manufacturer={searchManufacturer}
          setManufacturer={setSearchManufacturer}
        />
      </div>
    </form>
  );
}

export default SearchBar;
